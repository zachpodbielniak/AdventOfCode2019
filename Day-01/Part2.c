#include <PodNet/CFile/CFile.h>


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hFile;
	ARCHLONG alRead;
	CSTRING csBuffer[0x400];
	LPSTR lpszBuffer;
	ULONGLONG ullSum;
	ULONGLONG ullValue;

	hFile = OpenFile("./input", FILE_PERMISSION_READ, 0);
	if (NULL_OBJECT == hFile)
	{ return 1; }

	lpszBuffer = (LPSTR)csBuffer;
	ullSum = 0;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	while (-1 != (alRead = ReadLineFromFile(
		hFile,
		&lpszBuffer,
		sizeof(csBuffer) - 1,
		0
	))){
		if (NULLPTR != lpszBuffer)
		{
			StringScanFormat(
				lpszBuffer,
				"%llu",
				&ullValue
			);
			
			while (0 < (LONGLONG)((ullValue / 3) - 2))
			{
				ullValue /= 3;
				ullValue -= 2;
				ullSum += ullValue;
			}
		}

		ZeroMemory(csBuffer, sizeof(csBuffer));
	}

	PrintFormat("Sum: %llu\n", ullSum);
}
